import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  constructor(private http : HttpClient) { }
  private url: string = "api/4093375854065435";
  
  obtenerHeroes():Observable<any> {
    return this.http.get(this.url+"/search/s");
  }
  
  infoHeroe(id:any): Observable<any> {
    return this.http.get(this.url + "/"+id);
  }
}
