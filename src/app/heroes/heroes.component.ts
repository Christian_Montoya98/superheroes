import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../services/heroes.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  pageActual: number = 1;
  listHeroes: any[] = [];
  constructor(private heroesService:HeroesService) { }

  ngOnInit(): void {
     this.heroesService.obtenerHeroes().subscribe((res: any) => {
       console.log(res);
       this.listHeroes = res.results;
       
     }); 
  }

}
